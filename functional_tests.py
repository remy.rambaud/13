import os
import unittest
from time import sleep

from selenium import webdriver
from selenium.webdriver.common.keys import Keys


def connect_user(self, username):
    self.browser.get('http://localhost:8000/auth/signin')
    input_email = self.browser.find_element_by_id('id_email')
    input_password = self.browser.find_element_by_id('id_password')
    input_email.send_keys(username)
    input_password.send_keys('mail')
    sleep(2)
    input_password.send_keys(Keys.ENTER)
    sleep(2)


class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_homepage_access(self):
        # Sarah has heard about a online app that let her find a room.
        # She goes to check out its homepage
        self.browser.get('http://localhost:8000')

        # She notices the page title and header mention the name of the app
        self.assertIn('Ŝirmo', self.browser.title)


class GuestTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_user_connect(self):
        # Sarah goes to signup page and enter her credentials
        connect_user(self, 'sarah@mail.com')
        self.assertIn('messagerie', self.browser.page_source)

    def test_search(self):
        # Sarah wants to search a place in her city
        self.browser.get('http://localhost:8000/')
        input_search = self.browser.find_element_by_name('search')
        input_search.send_keys('CFTEST')
        input_search.send_keys(Keys.ENTER)
        sleep(2)
        self.assertIn('logements', self.browser.current_url)

    def test_create_guest_offer(self):
        # To obtain more visibilty, she wants to post an offer as guest
        image = os.path.abspath('sirmo/media/test.png')
        connect_user(self, 'sarah@mail.com')

        self.browser.get('http://localhost:8000/logements/ajouter/')
        sleep(2)

        input_guest_tab = self.browser.find_element_by_css_selector(
            '#pills-guest-tab')
        input_guest_tab.send_keys(Keys.ENTER)
        sleep(2)

        # Form fields
        input_title = self.browser.find_element_by_css_selector(
            'form#guest input#id_title')

        input_description = self.browser.find_element_by_css_selector(
            'form#guest textarea#id_description')
        input_city = self.browser.find_element_by_css_selector(
            'form#guest input#id_city')
        input_postal_code = self.browser.find_element_by_css_selector(
            'form#guest input#id_postal_code')
        input_image = self.browser.find_element_by_css_selector(
            'form#guest input#id_image')

        input_title.send_keys('Titre test')
        input_description.send_keys('Description test')
        input_city.send_keys('Ville test')
        input_postal_code.send_keys('0000')
        input_image.send_keys(image)

        self.browser.find_element_by_css_selector(
            'form#guest input[type="submit"]').click()

        self.assertEqual('http://localhost:8000/', self.browser.current_url)

    def test_send_message(self):
        # Sarah send a message to another user
        connect_user(self, 'sarah@mail.com')

        self.browser.get('http://localhost:8000/logements/details/h/61')

        link_to_message = self.browser.find_element_by_css_selector(
            'a[href="/messagerie/contacter/"]').click()

        self.assertIn('messagerie/contacter/', self.browser.current_url)

        input_subject = self.browser.find_element_by_css_selector(
            'input#id_subject')
        input_content = self.browser.find_element_by_css_selector(
            'textarea#id_content')

        input_subject.send_keys('Sujet test')
        input_content.send_keys('Message test')

        self.browser.find_element_by_css_selector(
            'input[type="submit"]').click()

        self.assertEqual('http://localhost:8000/messagerie/',
                         self.browser.current_url)


class HostTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_create_host_offer(self):
        # Idris have a home he wants to share,
        # he goes on Sirmo and post an offer as Host. 
        image = os.path.abspath('sirmo/media/test.png')
        connect_user(self, 'sarah@mail.com')

        self.browser.get('http://localhost:8000/logements/ajouter/')
        sleep(2)

        input_host_tab = self.browser.find_element_by_css_selector(
            '#pills-host-tab')
        input_host_tab.send_keys(Keys.ENTER)
        sleep(2)

        # Form fields
        input_title = self.browser.find_element_by_css_selector(
            'form#host input#id_title')

        input_description = self.browser.find_element_by_css_selector(
            'form#host textarea#id_description')
        input_city = self.browser.find_element_by_css_selector(
            'form#host input#id_city')
        input_street_name = self.browser.find_element_by_css_selector(
            'form#host input#id_street_name')
        input_street_number = self.browser.find_element_by_css_selector(
            'form#host input#id_street_number')
        input_postal_code = self.browser.find_element_by_css_selector(
            'form#host input#id_postal_code')
        input_image = self.browser.find_element_by_css_selector(
            'form#host input#id_image')

        input_title.send_keys('Titre test')
        input_description.send_keys('Description test')
        input_city.send_keys('Ville test')
        input_street_name.send_keys('Rue test')
        input_street_number.send_keys('00')
        input_postal_code.send_keys('0000')
        input_image.send_keys(image)

        self.browser.find_element_by_css_selector(
            'form#host input[type="submit"]').click()

        self.assertEqual('http://localhost:8000/', self.browser.current_url)


if __name__ == '__main__':
    unittest.main(warnings='ignore')
