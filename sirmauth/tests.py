from django.test import TestCase
from django.urls import resolve
from . import views
from django.contrib.auth.models import User


def create_user():
    User.objects.create_user('john@sign.in', 'john@sign.in', 'smith')


def signin_user(self):
    self.client.login(username='john@sign.in', password='smith')


def signout_user(self):
    self.client.logout()


class AuthPageTest(TestCase):
    def test_root_url(self):
        found = resolve('/auth/signin/')
        self.assertEqual(found.func, views.signin)

    def test_auth_signin(self):
        response = self.client.get('/auth/signin/')
        self.assertTemplateUsed(response, 'sirmauth/form_signin.html')

    def test_auth_signup(self):
        response = self.client.get('/auth/signup/')
        self.assertTemplateUsed(response, 'sirmauth/form_signup.html')


class UserTest(TestCase):
    def setUp(self):
        create_user()

    def tearDown(self):
        signout_user(self)
        User.objects.all().delete()

    def test_access_logged(self):
        signin_user(self)
        response = self.client.get('/logements/ajouter/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'offers/offers_add.html')

    def test_access_unlogged(self):
        signout_user(self)
        response = self.client.get('/logements/ajouter/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, '/auth/signin/?next=/logements/ajouter/')

