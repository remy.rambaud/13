from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

from sirmauth.forms import Sign, SignUp


def signup(request):
    form = SignUp()

    context = {
        'form': form,
    }

    if request.method == 'POST':
        form = SignUp(request.POST)

        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']

            if not User.objects.filter(username=email):
                user = User.objects.create_user(
                    first_name=first_name,
                    last_name=last_name,
                    username=email,
                    email=email,
                    password=password
                )

                # Authenticate and login user after registration.
                user = authenticate(username=form.cleaned_data['email'],
                                    password=form.cleaned_data['password'],)

                login(request, user)
                context['user_result'] = "Le compte a été créé !"
                return render(request, 'sirmauth/form_signin.html', context)

            else:
                context['user_result'] = "L'utilisateur existe déjà."

        else:
            context = {'form': form}

    return render(request, 'sirmauth/form_signup.html', context)


def signin(request):
    form = Sign()

    nextvalue = request.GET.get('next')

    context = {
        'form': form,
        'next': nextvalue,
    }

    if request.user.is_authenticated and nextvalue is not None:
        return redirect(nextvalue)

    else:
        if request.method == 'POST':
            form = Sign(request.POST)
            if form.is_valid():
                user = authenticate(username=form.cleaned_data['email'],
                                    password=form.cleaned_data['password'],)
                if user is not None:
                    login(request, user)
                    if nextvalue is not None:
                        return redirect(nextvalue)
                    else:
                        return redirect('index')
                else:
                    context['user_result'] = 'Identification invalide.'
                    return render(request, 'sirmauth/form_signin.html', context)
            else:
                context = {
                    'form': form,
                    'user_result': 'Identification invalide.',
                    'next': nextvalue,
                }

    return render(request, 'sirmauth/form_signin.html', context)


def signout(request):
    if not request.user.is_authenticated:
        return redirect('sirmauth:signin')
    logout(request)
    return redirect('index')
