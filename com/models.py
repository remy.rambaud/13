from django.contrib.auth.models import User
from django.db import models

from offers.models import Offer


class Topic(models.Model):
    id_offer = models.ForeignKey(Offer, on_delete=models.CASCADE)
    subject = models.CharField(max_length=50)
    id_sender = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='topic_sender')
    id_recipient = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='topic_recipient')

    def __str__(self):
        return self.subject


class Message(models.Model):
    id_sender = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='sender')
    id_topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    content = models.CharField(max_length=500)
    date = models.DateField(auto_now=False, auto_now_add=True)
