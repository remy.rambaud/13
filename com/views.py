from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import get_object_or_404, redirect, render

from com.forms import MessageForm
from com.models import Topic
from offers.models import Offer


@login_required(login_url='/auth/signin/')
def inbox(request):
    user = request.user

    # Get all topics related to current user
    topics = Topic.objects.filter(Q(id_sender=user) | Q(id_recipient=user))

    context = {
        'topics': topics
    }
    return render(request, 'com/inbox.html', context)


@login_required(login_url='/auth/signin/')
def contact(request):
    try:
        request.session['offer_id']
        request.session['offer_user']
    except KeyError:
        return redirect('/')

    if request.method == 'POST':
        form = MessageForm(request.POST)

        if form.is_valid():
            offer_id = request.session.pop('offer_id')
            offer_user_id = request.session.pop('offer_user')

            sender = request.user
            recipient = get_object_or_404(User, id=offer_user_id)
            offer = get_object_or_404(Offer, id=offer_id)

            subject = request.POST['subject']
            new_topic = Topic(
                id_offer=offer,
                id_sender=sender,
                id_recipient=recipient,
                subject=subject
            )
            new_topic.save()
            new_message = form.save(commit=False)
            new_message.id_topic = new_topic
            new_message.id_sender = sender
            new_message.save()

            return redirect('com:inbox')
    else:
        form = MessageForm()

    context = {
        'form': form,
    }

    return render(request, 'com/topic.html', context)


@login_required(login_url='/auth/signin/')
def message(request, id_topic):
    # From given topic, get all messages
    topic = get_object_or_404(Topic, id=id_topic)
    messages = topic.message_set.all()

    if request.method == 'POST':
        form = MessageForm(request.POST)

        if form.is_valid():
            user = request.user
            new_message = form.save(commit=False)
            new_message.id_topic = topic
            new_message.id_sender = user
            new_message.save()

            return redirect(request.path)
    else:
        form = MessageForm()

    context = {
        'topic': topic,
        'messages': messages,
        'form': form,
    }

    return render(request, 'com/message.html', context)
