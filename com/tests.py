from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import resolve, reverse

from offers.models import Guest, Host
from com.models import Topic, Message

from . import views


def create_user():
    User.objects.create_user('john@sign.in', 'john@sign.in', 'smith')
    User.objects.create_user('jeanne@sign.in', 'jeanne@sign.in', 'smith')


def signin_user(self):
    self.client.login(username='john@sign.in', password='smith')


def signout_user(self):
    self.client.logout()


def create_data():
    """ Create data in test database """
    offers_host_list = [
        {
            'title': 'Titre hôte',
            'description': 'Texte de description',
            'city': 'Grenoble',
            'street_name': 'Nom de rue',
            'street_number': '40',
            'postal_code': '38000',
            'image': 'image.png'
        },
    ]

    offers_guest_list = [
        {
            'title': 'Titre invité',
            'description': 'Texte de description',
            'city': 'Grenoble',
            'postal_code': '38000',
            'image': 'image.png'
        },
    ]

    topics_list = [
        {
            'subject': 'Sujet du message',
        },
    ]

    messages_list = [
        {
            'content': 'Contenu du message',
        },
    ]

    john = User.objects.get(username='john@sign.in')
    jeanne = User.objects.get(username='jeanne@sign.in')

    host_offer = john.offer_set.create()
    guest_offer = jeanne.offer_set.create()

    for offer in offers_host_list:
        host = Host.objects.create(
            id_offer=host_offer,
            **offer
        )

    for offer in offers_guest_list:
        host = Guest.objects.create(
            id_offer=guest_offer,
            **offer
        )

    for topic in topics_list:
        new_topic = Topic.objects.create(
            id_offer=host_offer,
            id_recipient=jeanne,
            id_sender=john,
            **topic
        )

    topic = Topic.objects.first()

    for message in messages_list:
        new_message = Message.objects.create(
            id_sender=john,
            id_topic=topic,
            **message
        )


class InboxTest(TestCase):
    def setUp(self):
        create_user()
        create_data()

    def tearDown(self):
        signout_user(self)
        User.objects.all().delete()

    def test_root_url(self):
        found = resolve('/messagerie/')
        self.assertEqual(found.func, views.inbox)

    def test_inbox(self):
        signin_user(self)
        response = self.client.get('/messagerie/')
        self.assertEqual(
            response.context['topics'][0].subject, 'Sujet du message'
        )

    def test_message(self):
        signin_user(self)
        topic = Topic.objects.first()
        response = self.client.get(reverse('com:message', args=[topic.id]))
        self.assertEqual(
            response.context['messages'][0].content, 'Contenu du message'
        )

    def test_message_send(self):
        signin_user(self)
        topic = Topic.objects.first()

        form_content = {
            'content': 'Réponse'}

        response_post = self.client.post(
            reverse('com:message', args=[topic.id]),
            form_content
        )

        response_get = self.client.get(
            reverse('com:message', args=[topic.id])
        )

        self.assertRedirects(
            response_post,
            reverse(
                'com:message', args=[topic.id]
            )
        )
        self.assertContains(response_get, 'Réponse')

    def test_first_message(self):
        signin_user(self)

        form_content = {
            'subject': 'Nouveau message',
            'content': 'Contenu'}

        offer_guest = Guest.objects.get(
            id_offer__id_user__username='jeanne@sign.in')

        session = self.client.session
        session['offer_id'] = offer_guest.id_offer_id
        session['offer_user'] = offer_guest.id_offer.id_user_id
        session.save()

        response = self.client.post(
            reverse('com:contact'),
            form_content
        )

        self.assertRedirects(response, reverse('com:inbox'))
