from django.forms import Form, ModelForm, Textarea
from com.models import Message, Topic

class MessageForm(ModelForm):
    class Meta:
        model = Message
        fields = ['content']

        labels = {'content': 'Message'}

        widgets = {
            'content': Textarea(
                attrs={
                    'cols': 40,
                    'rows': 10,
                    'class': 'form-control',
                    'placeholder': ' '
                }
            )
        }
