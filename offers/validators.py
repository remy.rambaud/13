from django.core.exceptions import ValidationError


def validate_file_size(value):
    filesize = value.size

    if filesize > 2000000:
        raise ValidationError("La taille maximale de l'image est de 2Mo.")
    else:
        return value
