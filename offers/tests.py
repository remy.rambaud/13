import os

from django.conf import settings
from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import resolve, reverse

from com.models import Message, Topic
from offers.models import Guest, Host, Offer

from . import views


def create_user():
    User.objects.create_user('john@sign.in', 'john@sign.in', 'smith')
    User.objects.create_user('jeanne@sign.in', 'jeanne@sign.in', 'smith')


def signin_user(self):
    self.client.login(username='john@sign.in', password='smith')


def signout_user(self):
    self.client.logout()


def create_data():
    """ Create data in test database """
    offers_host_list = [
        {
            'title': 'Titre hôte',
            'description': 'Texte de description',
            'city': 'Grenoble',
            'street_name': 'Nom de rue',
            'street_number': '40',
            'postal_code': '38000',
            'image': 'image.png'
        },
    ]

    offers_guest_list = [
        {
            'title': 'Titre invité',
            'description': 'Texte de description',
            'city': 'Grenoble',
            'postal_code': '38000',
            'image': 'image.png'
        },
    ]

    topics_list = [
        {
            'subject': 'Sujet du message',
        },
    ]

    messages_list = [
        {
            'content': 'Contenu du message',
        },
    ]

    john = User.objects.get(username='john@sign.in')
    jeanne = User.objects.get(username='jeanne@sign.in')

    host_offer = john.offer_set.create()
    guest_offer = jeanne.offer_set.create()

    for offer in offers_host_list:
        host = Host.objects.create(
            id_offer=host_offer,
            **offer
        )

    for offer in offers_guest_list:
        host = Guest.objects.create(
            id_offer=guest_offer,
            **offer
        )


class HostTest(TestCase):
    def setUp(self):
        create_user()
        create_data()

    def tearDown(self):
        signout_user(self)
        User.objects.all().delete()

    def test_root_url(self):
        found = resolve('/logements/')
        self.assertEqual(found.func, views.offers_host)

    def test_offers(self):
        response = self.client.get('/logements/')
        html = response.content.decode('utf8')
        self.assertTemplateUsed(response, 'offers/offers_list.html')

    def test_offers_search(self):
        response = self.client.post(
            reverse('offers:offers_host'),
            {'search': 'Grenoble'})
        self.assertEqual(
            response.context['offers_list'][0].city, 'Grenoble'
        )

    def test_offers_details(self):
        offer_host = Host.objects.get(
            id_offer__id_user__username='john@sign.in')
        response = self.client.get(
            reverse('offers:details_host', args=[offer_host.id]))
        self.assertEqual(
            response.context['offer_details'].city, 'Grenoble'
        )

    def test_offers_host_add(self):
        signin_user(self)

        file_path = os.path.join(settings.MEDIA_ROOT, 'test.png')
        with open(file_path, 'rb') as img:
            form_content = {
                'submit-host-form': 'True',
                'title': 'Titre annonce',
                'description': 'Description',
                'city': 'Grenoble',
                'street_name': 'Nom de rue',
                'street_number': '10',
                'postal_code': '38000',
                'image': img
            }

            response = self.client.post(
                reverse('offers:offers_add'), form_content)
            self.assertRedirects(response, '/')


class GuestTest(TestCase):
    def setUp(self):
        create_user()
        create_data()

    def tearDown(self):
        signout_user(self)
        User.objects.all().delete()

    def test_root_url(self):
        found = resolve('/logements/postulants/')
        self.assertEqual(found.func, views.offers_guest)

    def test_offers_add(self):
        signin_user(self)
        response = self.client.get('/logements/postulants/')
        self.assertTemplateUsed(response, 'offers/offers_list.html')

    def test_offers_search(self):
        response = self.client.post(
            reverse('offers:offers_guest'),
            {'search': 'Grenoble'})
        self.assertEqual(
            response.context['offers_list'][0].city, 'Grenoble'
        )

    def test_offers_details(self):
        offer_guest = Guest.objects.get(
            id_offer__id_user__username='jeanne@sign.in')
        response = self.client.get(
            reverse('offers:details_guest', args=[offer_guest.id]))
        self.assertEqual(
            response.context['offer_details'].city, 'Grenoble'
        )

    def test_offers_guest_add(self):
        signin_user(self)

        file_path = os.path.join(settings.MEDIA_ROOT, 'test.png')
        with open(file_path, 'rb') as img:
            form_content = {
                'submit-guest-form': 'True',
                'title': 'Titre annonce',
                'description': 'Description',
                'city': 'Grenoble',
                'postal_code': '38000',
                'image': img
            }

            response = self.client.post(
                reverse('offers:offers_add'), form_content)
            self.assertRedirects(response, '/')


class DetailsTest(TestCase):
    def setUp(self):
        create_user()
        create_data()

    def tearDown(self):
        User.objects.all().delete()
        Host.objects.all().delete()
        Guest.objects.all().delete()

    def test_access_details(self):
        signin_user(self)

        offer = Host.objects.first()
        response = self.client.get(
            reverse('offers:details_host', args=[offer.id])
        )

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'offers/details.html')
