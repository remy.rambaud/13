from django.forms import ModelForm, Textarea, TextInput
from offers.models import Host, Guest


class HostForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['title'].widget.attrs.update({'class': 'form-control'})
        self.fields['city'].widget.attrs.update({'class': 'form-control'})
        self.fields['street_name'].widget.attrs.update(
            {'class': 'form-control'})
        self.fields['street_number'].widget.attrs.update(
            {'class': 'form-control'})
        self.fields['postal_code'].widget.attrs.update(
            {'class': 'form-control'})
        self.fields['area'].widget.attrs.update({'class': 'form-control'})
        self.fields['room'].widget.attrs.update({'class': 'form-control'})
        self.fields['duration'].widget.attrs.update(
            {'class': 'form-control'})

    class Meta:
        model = Host
        fields = ['title',
                  'description',
                  'city',
                  'street_name',
                  'street_number',
                  'postal_code',
                  'area',
                  'room',
                  'duration',
                  'image']

        labels = {'title': 'Titre',
                  'description': 'Description',
                  'city': 'Ville',
                  'street_name': 'Rue',
                  'street_number': 'Numéro de rue',
                  'postal_code': 'Code postal',
                  'area': 'Surface habitable',
                  'room': 'Chambre individuelle',
                  'duration': 'Durée d\'hébergement'}

        widgets = {
            'description': Textarea(
                attrs={
                    'cols': 40,
                    'rows': 10,
                    'class': 'form-control',
                    'placeholder': 'Description'
                }
            ),
            'duration': TextInput(
                attrs={
                    'placeholder': 'Nombre de mois'
                }
            )
        }


class GuestForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['title'].widget.attrs.update({'class': 'form-control'})
        self.fields['city'].widget.attrs.update({'class': 'form-control'})
        self.fields['postal_code'].widget.attrs.update(
            {'class': 'form-control'})
        self.fields['duration'].widget.attrs.update(
            {'class': 'form-control'})

    class Meta:
        model = Guest
        fields = ['title',
                  'description',
                  'city',
                  'postal_code',
                  'duration',
                  'image'
                  ]

        labels = {'title': 'Titre',
                  'description': 'Description',
                  'city': 'Ville',
                  'postal_code': 'Code postal',
                  'duration': 'Durée d\'hébergement'
                  }

        widgets = {
            'description': Textarea(
                attrs={
                    'cols': 40,
                    'rows': 10,
                    'class': 'form-control',
                    'placeholder': 'Description'
                }
            )
        }
