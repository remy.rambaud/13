from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from offers.forms import HostForm, GuestForm
from django.db.models import Q

from offers.models import Offer, Host, Guest


def offers_host(request):
    context = {}

    if request.method == 'POST':
        search = request.POST['search']

        # Get all offers matching the search
        offers_list = Host.objects.filter(
            Q(city__icontains=search) | Q(postal_code__icontains=search))
        context['search'] = search
        context['offers_list'] = offers_list
    else:
        offers_list = Host.objects.all()
        context['offers_list'] = offers_list

    return render(request, 'offers/offers_list.html', context)


def offers_guest(request):
    context = {}

    if request.method == 'POST':
        search = request.POST['search']

        # Get all offers matching the search
        offers_list = Guest.objects.filter(
            Q(city__icontains=search) | Q(postal_code__icontains=search))
        context['search'] = search
        context['offers_list'] = offers_list
    else:
        offers_list = Guest.objects.all()
        context['offers_list'] = offers_list

    return render(request, 'offers/offers_list.html', context)


def details_host(request, offer_id):
    request.session['offer_id'] = None
    request.session['offer_user'] = None

    offer_details = get_object_or_404(Host, id=offer_id)
    offer_user = offer_details.id_offer.id_user

    # Keep data in memory for com.views.contact
    request.session['offer_id'] = offer_details.id_offer_id
    request.session['offer_user'] = offer_user.id

    context = {
        'offer_details': offer_details,
        'its_a_host': 'True',
    }

    return render(request, 'offers/details.html', context)


def details_guest(request, offer_id):
    request.session['offer_id'] = None
    request.session['offer_user'] = None

    offer_details = get_object_or_404(Guest, id=offer_id)
    offer_user = offer_details.id_offer.id_user

    # Keep data in memory for com.views.contact
    request.session['offer_id'] = offer_details.id_offer_id
    request.session['offer_user'] = offer_user.id

    context = {
        'offer_details': offer_details,
    }
    return render(request, 'offers/details.html', context)


@login_required(login_url='/auth/signin/')
def offers_add(request):
    if request.method == 'POST':

        # Create the right form instance according
        # to the choice of the user
        if 'submit-host-form' in request.POST:
            form = HostForm(request.POST, request.FILES)
        if 'submit-guest-form' in request.POST:
            form = GuestForm(request.POST, request.FILES)

        # Check whether it's valid:
        if form.is_valid():
            user = request.user
            new_offer = Offer.objects.create(id_user=user)
            form_offer = form.save(commit=False)
            form_offer.id_user_id = user
            form_offer.id_offer = new_offer
            form_offer.save()

            return redirect('/')

        # In case of invalid form, recreate context with
        # forms errors messages
        else:
            if 'submit-host-form' in request.POST:
                guest_form = GuestForm()
                context = {
                    'host_form': form,
                    'guest_form': guest_form,
                }

            if 'submit-guest-form' in request.POST:
                host_form = HostForm()
                context = {
                    'host_form': host_form,
                    'guest_form': form
                }

    # If a GET (or any other method) we'll create a blank form
    else:
        host_form = HostForm()
        guest_form = GuestForm()
        context = {
            'host_form': host_form,
            'guest_form': guest_form,
        }

    return render(request, 'offers/offers_add.html', context)
