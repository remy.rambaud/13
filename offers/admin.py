from django.contrib import admin
from offers.models import Host, Guest

# Register your models here.
# class HostAdmin(admin.ModelAdmin):
#     fields

admin.site.register(Host)
admin.site.register(Guest)
