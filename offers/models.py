from django.contrib.auth.models import User
from django.db import models

from offers.validators import validate_file_size

class Offer(models.Model):
    available = models.BooleanField(default=1)
    id_user = models.ForeignKey(User, on_delete=models.CASCADE)

class Host(models.Model):
    id_offer = models.ForeignKey(Offer, on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=400)
    city = models.CharField(max_length=50)
    street_name = models.CharField(max_length=50)
    street_number = models.CharField(max_length=5)
    postal_code = models.CharField(max_length=10)
    area = models.IntegerField(null=True, blank=True)
    room = models.BooleanField(null=True, blank=True)
    duration = models.IntegerField(null=True, blank=True)
    date_added = models.DateField(auto_now=False, auto_now_add=True)
    date_modified = models.DateField(auto_now=True, auto_now_add=False)
    image = models.ImageField(upload_to='', validators=[validate_file_size])

    def __str__(self):
        return self.title

    def roomstr(self):
        if self.room == 0:
            return "Non"
        else:
            return "Oui"


class Guest(models.Model):
    id_offer = models.ForeignKey(Offer, on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=400)
    city = models.CharField(max_length=50)
    postal_code = models.CharField(max_length=10)
    duration = models.IntegerField(null=True, blank=True)
    date_added = models.DateField(auto_now=False, auto_now_add=True)
    date_modified = models.DateField(auto_now=True, auto_now_add=False)
    image = models.ImageField(upload_to='', validators=[validate_file_size])

    def __str__(self):
        return self.title
