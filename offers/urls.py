"""sirmo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import views

app_name = 'offers'
urlpatterns = [
    path('', views.offers_host, name='offers_host'),
    path('postulants/', views.offers_guest, name='offers_guest'),
    path('ajouter/', views.offers_add, name='offers_add'),
    path('details/h/<offer_id>', views.details_host, name='details_host'),
    path('details/g/<offer_id>', views.details_guest, name='details_guest'),
]